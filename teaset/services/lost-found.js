import request from '@/common/request';
import Mock from 'mockjs';
const getEventListByUser = async function(uid, keywords = '', page = 1) {
	if (page > 3) return false;
	const data = Mock.mock({
		'eventList|3': [{
			id: '@increment()',
			"type|1": [
				"捡到",
				"丢失"
			],
			"status|1": [
				"已完成",
				""
			],
			address: '@county(true)',
			content: '@cparagraph()',
			time: "@date('yyyy-MM-dd')",
			"images|1-6": [
				'https://via.placeholder.com/350x150'
			]
		}]
	});
	return data.eventList;
}
const getEventList = async function(keywords = '', page = 1) {
	if (page > 3) return false;
	const data = Mock.mock({
		'eventList|3': [{
			id: '@increment()',
			user: {
				head_image: 'https://via.placeholder.com/80x80',
				name: '@cname()'
			},
			"type|1": [
				"捡到",
				"丢失"
			],
			"status|1": [
				"已完成",
				""
			],
			address: '@county(true)',
			content: '@cparagraph()',
			contact: /^1[38][1-9]\d{8}/,
			time: "@date('yyyy-MM-dd')",
			"images|1-6": [
				'https://via.placeholder.com/350x150'
			]
		}]
	});
	return data.eventList;
}

module.exports = {
	getEventList,
	getEventListByUser,
}
